<?php

declare(strict_types=1);

namespace UXF\Gen\Generator;

interface ClassNameConverter
{
    public function convert(string $className): string;

    public function resolveTag(string $className): ?string;
}
