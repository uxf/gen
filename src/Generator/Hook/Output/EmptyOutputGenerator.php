<?php

declare(strict_types=1);

namespace UXF\Gen\Generator\Hook\Output;

use UXF\Gen\Inspector\Schema\RouteSchema;

final readonly class EmptyOutputGenerator implements HookOutputGenerator
{
    public function generateHeaderString(): string
    {
        return '';
    }

    public function generateString(RouteSchema $rSchema): string
    {
        return '';
    }

    public static function getDefaultTypeName(): string
    {
        return 'empty';
    }
}
