<?php

declare(strict_types=1);

namespace UXF\Gen\Generator\Hook\Output;

use UXF\Gen\Inspector\Schema\RouteSchema;

interface HookOutputGenerator
{
    public function generateHeaderString(): string;

    public function generateString(RouteSchema $rSchema): string;

    public static function getDefaultTypeName(): string;
}
