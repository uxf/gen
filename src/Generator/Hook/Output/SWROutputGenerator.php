<?php

declare(strict_types=1);

namespace UXF\Gen\Generator\Hook\Output;

use Nette\Utils\Strings;
use UXF\Gen\Exception\GenException;
use UXF\Gen\Inspector\Schema\RouteSchema;
use UXF\Gen\Utils\TypeConverter;
use UXF\Gen\Utils\TypescriptHelper;

/**
 * @experimental
 */
final readonly class SWROutputGenerator implements HookOutputGenerator
{
    private const string Types = 'import useSWR from "swr";
import useSWRMutation from "swr/mutation";
import type { Key, SWRConfiguration } from "swr";
import type { SWRMutationConfiguration, SWRMutationResponse } from "swr/mutation";
import { stringify } from "qs";

export class ApiError extends Error {
    constructor(
        message: string,
        public readonly code: string,
        public readonly statusCode: number,
        public readonly info: {
            error: {
                code: string;
                message: string;
            },
            validationErrors: {
                field: string;
                message: string;
            }[]
        }
    ) {
        super(message);
    }
}

export const _url = (parts: readonly [string, any] | readonly [string]) => {
    const q = typeof parts[1] !== "undefined" ? stringify(parts[1], { skipNulls: true, addQueryPrefix: true }) : "";
    return `${parts[0]}${q}`;
}

export const _defaultFetch = (url: string, method: string = "GET", body: any = undefined) => fetch((process.env.API_URL ?? "") + url, { method, body: typeof body !== "undefined" ? JSON.stringify(body) : undefined }).then(async (res) => {
    const data = await res.json();

    if (!res.ok) {
        throw new ApiError(data.error.message, data.error.code, res.status, data);
    }

    return data;
});

declare global {
    const _customFetch: any;
}

const _fetch = typeof _customFetch === "function" ? _customFetch : _defaultFetch;

';

    public function __construct(
        private TypeConverter $typeConverter,
    ) {
    }

    public function generateHeaderString(): string
    {
        return self::Types;
    }

    public function generateString(RouteSchema $rSchema): string
    {
        $output = '';
        $usedNames = [];

        foreach ($rSchema->methods as $method => $secured) {
            $method = strtoupper($method);
            $routeName = $rSchema->name;
            $name = str_replace(' ', '', ucwords(Strings::replace($routeName, '/[^a-zA-Z0-9]/', ' ')));

            if (isset($usedNames[$name])) {
                throw new GenException("Duplicate route name: '$name'");
            }
            $usedNames[$name] = true;

            $urlHook = TypescriptHelper::generatePath($rSchema, 'config');

            $rb = $rSchema->requestBody !== null
                ? $this->typeConverter->convertToTypescript($rSchema->requestBody)
                : null;

            $rq = $rSchema->requestQueries !== []
                ? implode(' & ', array_map(fn ($query) => $this->typeConverter->convertToTypescript($query), $rSchema->requestQueries))
                : null;

            $rp = TypescriptHelper::resolvePathParams($this->typeConverter, $rSchema->pathParams);

            $r = $rSchema->response !== null
                ? $this->typeConverter->convertToTypescript($rSchema->response)
                : 'any';

            $hookParams = [];
            $mutationParams = [];
            if ($rb !== null) {
                $hookParams[] = "{ body: $rb }";
            }
            if ($rq !== null) {
                $hookParams[] = "{ query: $rq }";
                $mutationParams[] = "{ query: $rq }";
            }
            if ($rp !== null) {
                $hookParams[] = "{ path: $rp }";
                $mutationParams[] = "{ path: $rp }";
            }

            $keyName = "pick{$name}Key";
            $keyCall = "$keyName()";
            $keyConfig = '';
            $keyBody = $rq !== null ? "`$urlHook`, config.query" : "`$urlHook`";
            $url = "$keyName()[0]";
            if ($mutationParams !== []) {
                $keyCall = "$keyName(config)";
                $keyConfig = "config: " . implode(' & ', $mutationParams);
                $url = "_url($keyName(config))";
            }

            $output .= "// {$method} {$rSchema->path}\n";

            if ($rSchema->deprecated) {
                $output .= "/** @deprecated */\n";
            }
            $output .= "export const $keyName = ($keyConfig) => [$keyBody] as const;\n";
            if ($rSchema->deprecated) {
                $output .= "/** @deprecated */\n";
            }

            if ($method === 'GET') {
                $config = implode(' & ', [...$hookParams, '{ skip?: boolean; key?: Key }']);
                $config = $hookParams !== [] ? $config : '{ skip?: boolean; key?: Key } = {}';

                $output .= "export const use{$name}Query = <T extends SWRConfiguration<$r, ApiError>>(config: $config, options: T = {} as never) => useSWR<$r, ApiError, T>(config.skip !== true ? (config.key ?? {$keyCall}) : null, () => _fetch($url), options);";
            } else {
                $mutationConfig = implode(' & ', [...$mutationParams, '{ key?: Key }']);
                $mutationConfig = $mutationParams !== [] ? $mutationConfig : '{ key?: Key } = {}';
                $rbt = $rb ?? 'never';
                $mutationCall = $rb === null ? "() => _fetch($url, '$method')" : "(_: Key, { arg }: { arg: $rb}) => _fetch($url, '$method', arg)";

                $output .= "export const use{$name}Mutation = (config: $mutationConfig, options: SWRMutationConfiguration<$r, ApiError, Key, $rbt> | undefined = undefined): SWRMutationResponse<$r, ApiError, Key, $rbt> => useSWRMutation(config.key ?? {$keyCall}, $mutationCall, options);";
            }

            $output .= "\n";

            $config = implode(' & ', $hookParams);
            $config = $hookParams !== [] ? $config : '{} = {}';

            $name = $method === 'GET' ? "get{$name}" : lcfirst($name);
            $body = $rb !== null ? 'config.body' : 'undefined';

            if ($rSchema->deprecated) {
                $output .= "/** @deprecated */\n";
            }
            $output .= "export const $name = (config: $config): Promise<$r> => _fetch($url, '$method', $body);";
            $output .= "\n\n";
        }

        return $output;
    }

    public static function getDefaultTypeName(): string
    {
        return 'swr';
    }
}
