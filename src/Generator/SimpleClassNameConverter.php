<?php

declare(strict_types=1);

namespace UXF\Gen\Generator;

use Nette\Utils\Strings;
use UXF\Core\Utils\ClassNameHelper;

final readonly class SimpleClassNameConverter implements ClassNameConverter
{
    public function convert(string $className): string
    {
        // uxf internal stuff
        $matches = Strings::match($className, '/^UXF\\\\(\\w+)\\\\/i');
        if ($matches !== null && !str_contains($matches[1], 'Tests')) {
            $name = ClassNameHelper::shortName($className);
            $zone = $matches[1];
            return str_starts_with($name, $zone) || $zone === 'Core' ? $name : $zone . $name;
        }

        $zone = $this->resolveTag($className);
        if ($zone !== null) {
            $sections = explode('\\', $className);
            $name = end($sections);
            return str_starts_with($name, $zone) ? $name : $zone . $name;
        }

        return trim(str_replace('\\', '_', $className), '_');
    }

    public function resolveTag(string $className): ?string
    {
        $sections = explode('\\', $className);
        foreach ($sections as $section) {
            if (str_ends_with($section, 'Zone')) {
                return Strings::replace($section, '/Zone$/');
            }
        }

        return null;
    }
}
