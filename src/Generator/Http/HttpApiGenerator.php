<?php

declare(strict_types=1);

namespace UXF\Gen\Generator\Http;

use UXF\Gen\Exception\GenException;
use UXF\Gen\Generator\ClassNameConverter;
use UXF\Gen\Inspector\Schema\TypeSchema;
use UXF\Gen\Inspector\Schema\TypeVariant;
use UXF\Gen\Inspector\TypeMap;
use UXF\Gen\Utils\GenericHelper;
use UXF\Gen\Utils\TypeConverter;

final readonly class HttpApiGenerator
{
    public function __construct(
        private TypeConverter $typeConverter,
        private ClassNameConverter $classNameConverter,
        private EnumGenerator $enumGenerator,
    ) {
    }

    public function generateString(TypeMap $typeSchemas): string
    {
        $used = [];
        $output = '';

        // enums
        foreach ($typeSchemas as $typeSchema) {
            if ($typeSchema->variant === TypeVariant::ENUM && !isset($used[$typeSchema->name])) {
                $output .= $this->enumGenerator->generateString($typeSchema->name, $typeSchema);
                $used[$typeSchema->name] = true;
            }
        }

        $usedShortClassNames = [];
        $usedGenericClassNames = [];

        foreach ($typeSchemas as $typeSchema) {
            if (isset($used[$typeSchema->name])) {
                continue;
            }
            $used[$typeSchema->name] = true;

            if ($typeSchema->variant === TypeVariant::SIMPLE) {
                $tsType = $this->typeConverter->convertToTypescriptType($typeSchema);
                $output .= "type {$tsType->name} = {$tsType->type};";
                $output .= "\n\n";
                continue;
            }

            if ($typeSchema->variant === TypeVariant::DISCRIMINATOR_OBJECT) {
                $className = $this->classNameConverter->convert($typeSchema->name);

                $output .= "// {$typeSchema->comment}\n";
                $output .= "export type {$className} = ";

                $output .= implode(' | ', array_map(fn (TypeSchema $child) => $this->classNameConverter->convert($child->name), $typeSchema->children));
                $output .= ";\n\n";
            }

            // generic
            if ($typeSchema->variant === TypeVariant::GENERIC_OBJECT) {
                [$typeName] = GenericHelper::resolve($typeSchema->name);
                if (isset($usedGenericClassNames[$typeName])) {
                    continue;
                }
                $usedGenericClassNames[$typeName] = true;

                $className = $this->classNameConverter->convert($typeName);
                $output .= "// $typeName\n";
                $output .= "export interface {$className}<T> {\n";

                foreach ($typeSchema->properties as $property) {
                    if ($property->generic) {
                        $output .= "    {$property->name}: T;\n";
                        continue;
                    }

                    $type = $this->typeConverter->convertToTypescript($property);

                    $optional = $property->optional ? '?' : '';
                    $output .= "    {$property->name}{$optional}: $type";

                    if ($property->nullable) {
                        $output .= ' | null';
                    }
                    $output .= ";\n";
                }

                $output .= "}\n\n";

                continue;
            }

            if ($typeSchema->variant !== TypeVariant::OBJECT) {
                continue;
            }

            // check unique final classname
            $className = $this->classNameConverter->convert($typeSchema->name);
            $alreadyUsedClassname = $usedShortClassNames[$className] ?? null;
            if ($alreadyUsedClassname !== null) {
                throw new GenException("Duplicate short classname '$className' ({$typeSchema->name} + $alreadyUsedClassname)");
            }
            $usedShortClassNames[$className] = $typeSchema->name;

            $output .= "// {$typeSchema->comment}\n";
            if ($typeSchema->deprecated) {
                $output .= "/** @deprecated */\n";
            }
            $output .= "export interface {$className} {\n";

            foreach ($typeSchema->properties as $property) {
                $type = $this->typeConverter->convertToTypescript($property);

                if ($property->deprecated) {
                    $output .= "    /** @deprecated */\n";
                }

                $optional = $property->optional ? '?' : '';
                $output .= "    {$property->name}{$optional}: $type";

                if ($property->nullable) {
                    $output .= ' | null';
                }
                $output .= ";\n";
            }

            $output .= "}\n\n";
        }

        return $output;
    }
}
