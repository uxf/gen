<?php

declare(strict_types=1);

namespace UXF\Gen\Generator\Http;

use UXF\Gen\Inspector\Schema\TypeSchema;

interface EnumGenerator
{
    public function generateString(string $typeName, TypeSchema $typeSchema): string;
}
