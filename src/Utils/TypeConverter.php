<?php

declare(strict_types=1);

namespace UXF\Gen\Utils;

use LogicException;
use Ramsey\Uuid\UuidInterface;
use UXF\Core\Utils\ClassNameHelper;
use UXF\Gen\Exception\GenException;
use UXF\Gen\Generator\ClassNameConverter;
use UXF\Gen\Inspector\Schema\ArrayVariant;
use UXF\Gen\Inspector\Schema\PropertySchema;
use UXF\Gen\Inspector\Schema\TypeSchema;
use UXF\Gen\Inspector\Schema\TypeVariant;
use UXF\Gen\Plugin\TypeConverterPlugin;
use UXF\Gen\Plugin\TypescriptType;

final readonly class TypeConverter
{
    /**
     * @param iterable<TypeConverterPlugin> $plugins
     * @param array<string, string> $types
     */
    public function __construct(
        private ClassNameConverter $classNameConverter,
        private array $types,
        private iterable $plugins = [],
    ) {
    }

    public function convertToTypescript(PropertySchema $propertySchema): string
    {
        $types = [];

        foreach ($propertySchema->types as $typeSchema) {
            foreach ($this->plugins as $plugin) {
                $winner = $plugin->convertToTypescript($typeSchema);
                if ($winner !== null) {
                    $types[] = $winner->name;
                    continue 2;
                }
            }

            $types[] = match ($typeSchema->variant) {
                TypeVariant::OBJECT, TypeVariant::DISCRIMINATOR_OBJECT => $this->classNameConverter->convert($typeSchema->name),
                TypeVariant::ENUM => ClassNameHelper::shortName($typeSchema->name),
                TypeVariant::DOCTRINE_OBJECT, TypeVariant::SIMPLE => $this->convertToTypescriptType($typeSchema)->name,
                TypeVariant::GENERIC_OBJECT => call_user_func(function (string $typeName) {
                    [$typeName, $innerGeneticTypeName, $innerGeneticIsArray] = GenericHelper::resolve($typeName);

                    $t1 = $this->classNameConverter->convert($typeName);
                    $t2 = $this->classNameConverter->convert($innerGeneticTypeName ?? throw new LogicException());

                    $t2 = match ($t2) {
                        'int' => 'Int',
                        'float' => 'Float',
                        'bool' => 'Bool',
                        'string' => 'XString',
                        UuidInterface::class => 'UUID',
                        'mixed' => 'Mixed',
                        default => $t2,
                    };

                    return $innerGeneticIsArray ? "$t1<Array<$t2>>" : "$t1<$t2>";
                }, $typeSchema->name),
            };
        }

        $type = implode(' | ', $types);

        if ($propertySchema->discriminatorValue !== null) {
            $type = "\"$propertySchema->discriminatorValue\"";
        }

        return match ($propertySchema->array) {
            ArrayVariant::NONE => $type,
            ArrayVariant::LIST => "Array<$type>",
            ArrayVariant::INDEXED => "Record<string, $type>",
        };
    }

    public function convertToTypescriptType(TypeSchema $typeSchema): TypescriptType
    {
        foreach ($this->plugins as $plugin) {
            $winner = $plugin->convertToTypescript($typeSchema);
            if ($winner !== null) {
                return $winner;
            }
        }

        return match ($typeSchema->name) {
            'int' => new TypescriptType('Int', $this->types['Int'] ?? 'number'),
            'float' => new TypescriptType('Float', $this->types['Float'] ?? 'number'),
            'bool' => new TypescriptType('Bool', $this->types['Bool'] ?? 'boolean'),
            'string' => new TypescriptType('XString', $this->types['XString'] ?? 'string'),
            UuidInterface::class => new TypescriptType('UUID', $this->types['UUID'] ?? 'string'),
            'mixed' => new TypescriptType('Mixed', $this->types['Mixed'] ?? 'any'),
            default => throw new GenException($typeSchema->name),
        };
    }

    /**
     * @return mixed[]
     */
    public function convertToOpenApi(PropertySchema $propertySchema): array
    {
        $types = [];
        foreach ($propertySchema->types as $typeSchema) {
            foreach ($this->plugins as $plugin) {
                $winner = $plugin->convertToOpenApi($typeSchema);
                if ($winner !== null) {
                    $types[] = $winner;
                    continue 2;
                }
            }

            $typeName = $typeSchema->name;

            $types[] = match ($typeSchema->variant) {
                TypeVariant::OBJECT, TypeVariant::DISCRIMINATOR_OBJECT => [
                    '$ref' => '#/components/schemas/' . $this->classNameConverter->convert($typeName),
                ],
                TypeVariant::ENUM => [
                    'type' => is_string($typeName::cases()[0]->value) ? 'string' : 'integer',
                    'enum' => array_map(static fn ($item) => $item->value, $typeName::cases()),
                ],
                TypeVariant::GENERIC_OBJECT => call_user_func(function (TypeSchema $typeSchema): array {
                    $properties = [];
                    $required = [];
                    foreach ($typeSchema->properties as $property) {
                        $properties[$property->name] = $this->convertToOpenApi($property);
                        if (!$property->optional) {
                            $required[] = $property->name;
                        }
                    }

                    $result = [
                        'properties' => $properties,
                    ];

                    if ($required !== []) {
                        $result['required'] = $required;
                    }

                    return $result;
                }, $typeSchema),
                TypeVariant::DOCTRINE_OBJECT, TypeVariant::SIMPLE => match ($typeName) {
                    'int' => [
                        'type' => 'integer',
                    ],
                    'float' => [
                        'type' => 'number',
                    ],
                    'bool' => [
                        'type' => 'boolean',
                    ],
                    UuidInterface::class => [
                        'type' => 'string',
                        'format' => 'uuid',
                    ],
                    'string' => [
                        'type' => 'string',
                    ],
                    'mixed' => [
                        'type' => 'object',
                    ],
                    default => throw new GenException($typeName),
                },
            };
        }

        $schema = match (true) {
            $propertySchema->array === ArrayVariant::INDEXED => [
                'type' => 'object',
                'additionalProperties' => count($types) === 1 ? $types[0] : [
                    'oneOf' => $types,
                ],
            ],
            $propertySchema->array === ArrayVariant::LIST => [
                'type' => 'array',
                'items' => count($types) === 1 ? $types[0] : [
                    'oneOf' => $types,
                ],
            ],
            count($types) !== 1 => [
                'oneOf' => $types,
            ],
            default => $types[0],
        };

        if ($propertySchema->nullable) {
            // $ref + nullable is not allowed in 3.0 -> convert to oneOf
            if (isset($schema['$ref'])) {
                $schema['oneOf'] = [[
                    '$ref' => $schema['$ref'],
                ]];
                unset($schema['$ref']);
            }

            $schema['nullable'] = true;
        }

        return $schema;
    }
}
