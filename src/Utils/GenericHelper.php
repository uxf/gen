<?php

declare(strict_types=1);

namespace UXF\Gen\Utils;

use Nette\Utils\Strings;

final readonly class GenericHelper
{
    /**
     * @return array{class-string, class-string|null, bool}
     */
    public static function resolve(string $typeName): array
    {
        $m = Strings::match($typeName, '/^(.*):([\w\d\\\\]+)(\[\])?$/i');
        return $m !== null ? [$m[1], $m[2], isset($m[3])] : [$typeName, null, false];
    }
}
