<?php

declare(strict_types=1);

namespace UXF\Gen\Utils;

use Nette\Utils\Strings;
use UXF\Gen\Inspector\Schema\PropertySchema;
use UXF\Gen\Inspector\Schema\RouteSchema;

final readonly class TypescriptHelper
{
    public static function generatePath(RouteSchema $routeSchema, string $rootName): string
    {
        $prefix = $rootName . '.path.';

        $path = Strings::replace($routeSchema->path, '/({)/', '\$$1' . $prefix);

        foreach ($routeSchema->pathParams as $pathParam) {
            if (isset($pathParam->types['bool'])) {
                $n = $pathParam->name;
                $path = str_replace('{' . $prefix . "$n}", '{' . $prefix . "$n ? 1 : 0}", $path);
            }
        }

        return $path;
    }

    /**
     * @param PropertySchema[] $pathParams
     */
    public static function resolvePathParams(TypeConverter $typeConverter, array $pathParams): ?string
    {
        if ($pathParams === []) {
            return null;
        }

        $result = implode(', ', array_map(static fn (PropertySchema $ps) => "$ps->name: " . $typeConverter->convertToTypescript($ps), $pathParams));

        return "{ $result }";
    }
}
