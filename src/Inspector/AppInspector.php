<?php

declare(strict_types=1);

namespace UXF\Gen\Inspector;

use UXF\Gen\Inspector\Schema\AppSchema;
use UXF\Gen\Plugin\InspectorPlugin;

final readonly class AppInspector
{
    /**
     * @param iterable<InspectorPlugin> $plugins
     */
    public function __construct(
        private RouteInspector $routeInspector,
        private iterable $plugins = [],
    ) {
    }

    public function inspect(string $configName, string $pathPattern): AppSchema
    {
        $types = new TypeMap();

        foreach ($this->plugins as $plugin) {
            $plugin->pre($configName, $types);
        }

        $routes = $this->routeInspector->inspect($types, $pathPattern);
        $types->sort();
        $appSchema = new AppSchema($routes, $types);

        foreach ($this->plugins as $plugin) {
            $plugin->post($configName, $appSchema);
        }

        return $appSchema;
    }
}
