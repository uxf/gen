<?php

declare(strict_types=1);

namespace UXF\Gen\Inspector;

use Deprecated;
use JetBrains\PhpStorm\Deprecated as JetBrainsDeprecated;
use LogicException;
use PHPStan\PhpDocParser\Parser\ParserException;
use Psr\Http\Message\ResponseInterface;
use ReflectionAttribute;
use ReflectionClass;
use ReflectionMethod;
use ReflectionNamedType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadataFactory;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authorization\Voter\AuthenticatedVoter;
use Symfony\Component\Security\Http\AccessMapInterface;
use UXF\Gen\Config\RouteInspectorConfig;
use UXF\Gen\Exception\GenException;
use UXF\Gen\Inspector\Schema\ArrayVariant;
use UXF\Gen\Inspector\Schema\PropertySchema;
use UXF\Gen\Inspector\Schema\RouteSchema;
use UXF\Gen\Inspector\Schema\TypeSchema;
use UXF\Gen\Inspector\Schema\TypeVariant;
use function Safe\preg_match;
use function Safe\preg_match_all;

final readonly class RouteInspector
{
    public function __construct(
        private string $globalPrefix,
        private RouteInspectorConfig $config,
        private RouterInterface $router,
        private MethodInspector $methodInspector,
        private TypeInspector $typeInspector,
        private PhpDocInspector $phpDocInspector,
        private AccessMapInterface $accessMap,
        private ArgumentMetadataFactory $argumentMetadataFactory = new ArgumentMetadataFactory(),
    ) {
    }

    /**
     * @return RouteSchema[]
     */
    public function inspect(TypeMap $typeMap, ?string $pathPattern = null): array
    {
        /** @var RouteSchema[] $routeSchemas */
        $routeSchemas = [];

        $routes = $this->router->getRouteCollection();
        foreach ($routes as $routeName => $route) {
            $path = $route->getPath();
            if (
                !str_starts_with($path, $this->globalPrefix) ||
                str_starts_with($path, "{$this->globalPrefix}/doc/")
            ) {
                continue;
            }

            if ($pathPattern !== null && preg_match($pathPattern, $path) === 0) {
                continue;
            }

            $controller = $route->getDefault('_controller');
            if (is_array($controller)) {
                [$contrClass, $contrMethod] = $controller;
            } elseif (str_contains($controller, '::')) {
                [$contrClass, $contrMethod] = explode('::', $controller);
            } else {
                $contrClass = $controller;
                $contrMethod = '__invoke';
            }

            if (!class_exists($contrClass)) {
                continue;
            }

            $reflectionClass = new ReflectionClass($contrClass);
            $reflectionMethod = $reflectionClass->getMethod($contrMethod);

            $deprecated = $reflectionClass->getAttributes(JetBrainsDeprecated::class) !== [] || $reflectionClass->getAttributes(Deprecated::class) !== [];
            $deprecated = $deprecated || str_contains((string) $reflectionClass->getDocComment(), '@deprecated');
            $deprecated = $deprecated || $reflectionMethod->getAttributes(JetBrainsDeprecated::class) !== [] || $reflectionMethod->getAttributes(Deprecated::class) !== [];
            $deprecated = $deprecated || str_contains((string) $reflectionMethod->getDocComment(), '@deprecated');

            try {
                $description = $this->phpDocInspector->getPhpDocComment((string) $reflectionMethod->getDocComment(), '@description') ?? '';
            } catch (ParserException) {
                $description = '';
            }

            $routeSchemas[] = $routeSchema = new RouteSchema(
                name: $routeName,
                path: $path,
                controller: $contrClass,
                requestBody: $this->getRequestBody($typeMap, $contrClass, $contrMethod, $this->config->bodyAttribute),
                requestQueries: $this->getRequestQueriesOrHeaders($typeMap, $contrClass, $contrMethod, $this->config->queryAttribute),
                requestHeaders: $this->getRequestQueriesOrHeaders($typeMap, $contrClass, $contrMethod, $this->config->headerAttribute),
                pathParams: $this->getPathParameters($typeMap, $contrClass, $contrMethod, $path),
                response: $this->getResponse($typeMap, $reflectionMethod),
                description: $description,
                deprecated: $deprecated,
            );

            foreach ($route->getMethods() as $method) {
                $routeSchema->methods[mb_strtolower($method)] = $this->isSecured($method, $path);
            }
        }

        return $routeSchemas;
    }

    /**
     * @return PropertySchema[]
     */
    private function getPathParameters(TypeMap $typeMap, string $contrClass, string $contrMethod, string $path): array
    {
        $metadatas = $this->argumentMetadataFactory->createArgumentMetadata([$contrClass, $contrMethod], new ReflectionMethod($contrClass, $contrMethod));

        $parameters = [];

        preg_match_all("/\{([^\}]*)\}/", $path, $m);
        $allowedParameterNames = $m[1] ?? [];

        foreach ($metadatas as $metadata) {
            $name = $metadata->getName();
            if (!in_array($name, $allowedParameterNames, true)) {
                continue;
            }

            $originalType = $metadata->getType();
            if ($originalType === null) {
                continue;
            }

            if (class_exists($originalType)) {
                // ignore request body, query, header object
                if (
                    $metadata->getAttributes($this->config->bodyAttribute, ReflectionAttribute::IS_INSTANCEOF) !== [] ||
                    $metadata->getAttributes($this->config->queryAttribute, ReflectionAttribute::IS_INSTANCEOF) !== [] ||
                    $metadata->getAttributes($this->config->headerAttribute, ReflectionAttribute::IS_INSTANCEOF) !== []
                ) {
                    continue;
                }
            }

            if (isset($typeMap[$originalType])) {
                $typeName = $originalType;
                $typeSchema = $typeMap[$originalType];
            } elseif (enum_exists($originalType)) {
                // register new enum type
                $typeName = $originalType;
                $typeSchema = $this->inspectInner($typeMap, $typeName, null);
            } else {
                $typeName = null;
                $typeSchema = null;
            }

            $entityAttribute = $metadata->getAttributes($this->config->entityAttribute, ReflectionAttribute::IS_INSTANCEOF)[0] ?? null;
            if ($entityAttribute !== null && is_a($entityAttribute, $this->config->entityAttribute, true)) {
                if (!property_exists($entityAttribute, 'property')) {
                    throw new GenException("Missing {$this->config->entityAttribute}::\$property property");
                }
                $typeSchema = $this->inspectInner($typeMap, $originalType, $entityAttribute->property);
            } elseif (
                ($typeSchema === null && class_exists($originalType)) ||
                $typeSchema?->variant === TypeVariant::DOCTRINE_OBJECT
            ) {
                // convert entity
                $typeSchema = $this->inspectInner($typeMap, $originalType, null);
            }

            if ($typeSchema === null) {
                continue;
            }

            $parameters[$name] = new PropertySchema(
                name: $name,
                types: [
                    $typeName => $typeSchema,
                ],
                array: ArrayVariant::NONE,
                nullable: false,
                optional: false,
            );
        }

        return $parameters;
    }

    /**
     * @param class-string $attributeClass
     */
    private function getRequestBody(TypeMap $typeMap, string $contrClass, string $contrMethod, string $attributeClass): ?PropertySchema
    {
        $metadatas = $this->argumentMetadataFactory->createArgumentMetadata([$contrClass, $contrMethod], new ReflectionMethod($contrClass, $contrMethod));

        $httpType = null;
        $isArray = false;
        foreach ($metadatas as $metadata) {
            $type = $metadata->getType();
            $isArray = $type === 'array';

            $attribute = $metadata->getAttributes($attributeClass, ReflectionAttribute::IS_INSTANCEOF)[0] ?? null;
            if ($attribute !== null && is_a($attribute, $this->config->bodyAttribute, true)) {
                if (!property_exists($attribute, 'arrayClassName')) {
                    throw new GenException("Missing $attributeClass::\$arrayClassName property");
                }
                /** @var string|null $httpType */
                $httpType = $isArray ? $attribute->arrayClassName : $type;
                break;
            }
        }

        if ($httpType === null) {
            return null;
        }

        if (!isset($typeMap[$httpType])) {
            // register new type
            $this->inspectInner($typeMap, $httpType, null);
        }

        if (!isset($typeMap[$httpType])) {
            throw new GenException("Missing type $httpType in controller $contrClass::$contrMethod");
        }

        return new PropertySchema(
            name: '',
            types: [
                $httpType => $typeMap[$httpType],
            ],
            array: $isArray ? ArrayVariant::LIST : ArrayVariant::NONE,
            nullable: false,
            optional: false,
        );
    }

    /**
     * @param class-string $attributeClass
     * @return PropertySchema[]
     */
    private function getRequestQueriesOrHeaders(TypeMap $typeMap, string $contrClass, string $contrMethod, string $attributeClass): array
    {
        $metadatas = $this->argumentMetadataFactory->createArgumentMetadata([$contrClass, $contrMethod], new ReflectionMethod($contrClass, $contrMethod));

        $types = [];
        foreach ($metadatas as $metadata) {
            $type = $metadata->getType();
            $attribute = $metadata->getAttributes($attributeClass, ReflectionAttribute::IS_INSTANCEOF)[0] ?? null;
            if ($attribute !== null && $type !== null) {
                if (
                    is_a($attribute, $this->config->queryAttribute, true) ||
                    is_a($attribute, $this->config->headerAttribute, true)
                ) {
                    if (!isset($typeMap[$type])) {
                        // register new type
                        $this->inspectInner($typeMap, $type, null);
                    }

                    if (!isset($typeMap[$type])) {
                        throw new GenException("Missing type $type in controller $contrClass::$contrMethod");
                    }

                    $types[] = new PropertySchema(
                        name: '',
                        types: [
                            $type => $typeMap[$type],
                        ],
                        array: ArrayVariant::NONE,
                        nullable: false,
                        optional: false,
                    );
                }
            }
        }

        return $types;
    }

    private function getResponse(TypeMap $typeMap, ReflectionMethod $reflectionMethod): PropertySchema
    {
        $reflectionReturnType = $reflectionMethod->getReturnType();
        if ($reflectionReturnType instanceof ReflectionNamedType) {
            $name = $reflectionReturnType->getName();
            if (
                $name === 'void' ||
                is_a($name, Response::class, true) ||
                is_a($name, ResponseInterface::class, true)
            ) {
                return new PropertySchema(
                    name: '',
                    types: [
                        'mixed' => $typeMap['mixed'] ?? throw new LogicException(),
                    ],
                    array: ArrayVariant::NONE,
                    nullable: false,
                    optional: false,
                );
            }
        }

        return $this->methodInspector->inspectReturnType($typeMap, '', $reflectionMethod);
    }

    private function inspectInner(TypeMap $typeMap, string $typeName, ?string $doctrineProperty): TypeSchema
    {
        return $this->typeInspector->inspect($typeMap, $typeName, $doctrineProperty);
    }

    private function isSecured(string $method, string $path): bool
    {
        $request = Request::create($path, $method);
        [$attrs] = $this->accessMap->getPatterns($request);
        return $attrs !== null && [AuthenticatedVoter::PUBLIC_ACCESS] !== $attrs;
    }
}
