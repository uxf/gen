<?php

declare(strict_types=1);

namespace UXF\Gen\Inspector;

use BackedEnum;
use Deprecated;
use JetBrains\PhpStorm\Deprecated as JetBrainsDeprecated;
use LogicException;
use ReflectionClass;
use ReflectionException;
use ReflectionNamedType;
use ReflectionParameter;
use ReflectionProperty;
use UXF\Gen\Exception\GenException;
use UXF\Gen\Inspector\Schema\ArrayVariant;
use UXF\Gen\Inspector\Schema\PropertySchema;
use UXF\Gen\Inspector\Schema\TypeSchema;
use UXF\Gen\Inspector\Schema\TypeVariant;
use UXF\Gen\Utils\GenericHelper;
use UXF\Hydrator\Attribute\HydratorMap;
use function Safe\json_encode;

final readonly class TypeInspector
{
    public function __construct(private PhpDocInspector $phpDocInspector)
    {
    }

    public function inspect(TypeMap $typeMap, string $typeName, ?string $property = null, ?HydratorMap $discriminator = null): TypeSchema
    {
        if (isset($typeMap[$typeName])) {
            if ($discriminator !== null) {
                $tmpProperty = $this->discToPropertySchema($typeMap, $discriminator, $typeName);
                $typeMap[$typeName]->properties[$tmpProperty->name] = $tmpProperty;
            }

            return $typeMap[$typeName];
        }

        $originalTypeName = $typeName;
        [$typeName, $innerGeneticTypeName, $innerGeneticIsArray] = GenericHelper::resolve($typeName);

        if (enum_exists($typeName)) {
            return $typeMap[$typeName] = new TypeSchema($typeName, TypeVariant::ENUM, $typeName);
        }

        $doctrineKeyType = $this->phpDocInspector->inspectPropertyType($typeName, $property);
        if ($doctrineKeyType !== null) {
            return $typeMap["$typeName#$doctrineKeyType"] = enum_exists($doctrineKeyType)
                ? new TypeSchema($doctrineKeyType, TypeVariant::ENUM, $doctrineKeyType)
                : new TypeSchema($doctrineKeyType, TypeVariant::SIMPLE);
        }

        if (!class_exists($typeName) && !interface_exists($typeName)) {
            return $typeMap['mixed'] ?? throw new LogicException();
        }

        $reflectionClass = new ReflectionClass($typeName);
        $deprecated = $reflectionClass->getAttributes(JetBrainsDeprecated::class) !== [] || $reflectionClass->getAttributes(Deprecated::class) !== [];
        $deprecated = $deprecated || str_contains((string) $reflectionClass->getDocComment(), '@deprecated');

        $mapAttribute = $reflectionClass->getAttributes(HydratorMap::class)[0] ?? null;
        if ($mapAttribute !== null) {
            /** @var HydratorMap $hydratorMap */
            $hydratorMap = $mapAttribute->newInstance();

            $children = [];
            foreach ($hydratorMap->matrix as $p => $class) {
                $children[$p] = $this->inspect($typeMap, $class, discriminator: $hydratorMap);
            }

            return $typeMap[$typeName] = new TypeSchema(
                name: $typeName,
                variant: TypeVariant::DISCRIMINATOR_OBJECT,
                comment: $typeName,
                properties: [],
                deprecated: $deprecated,
                discriminatorProperty: $hydratorMap->property,
                children: $children,
            );
        }

        $innerGeneticTypes = [];
        if ($innerGeneticTypeName !== null) {
            $innerGeneticTypes = [
                $innerGeneticTypeName => $this->inspect($typeMap, $innerGeneticTypeName),
            ];
            $typeSchema = new TypeSchema($originalTypeName, TypeVariant::GENERIC_OBJECT, $originalTypeName, deprecated: $deprecated);
        } else {
            $typeSchema = new TypeSchema($typeName, TypeVariant::OBJECT, $typeName, deprecated: $deprecated);
        }

        $typeMap[$originalTypeName] = $typeSchema;

        $constructorReflectionMethod = $reflectionClass->getConstructor();
        if (null === $constructorReflectionMethod) {
            throw new GenException("Class '$typeName' is missing constructor");
        }

        if (!$constructorReflectionMethod->isPublic()) {
            throw new GenException("Class '$typeName' has private constructor");
        }

        foreach ($constructorReflectionMethod->getParameters() as $reflectionParameter) {
            $propertyName = $reflectionParameter->getName();
            if ($reflectionParameter->isPromoted()) {
                $reflectionProperty = new ReflectionProperty($typeName, $propertyName);
                if (!$reflectionProperty->isPublic()) {
                    continue;
                }
            }

            if ($innerGeneticTypes !== [] && str_contains($constructorReflectionMethod->getDocComment() . '', "@param T \$$propertyName")) {
                $propertySchema = new PropertySchema(
                    name: $propertyName,
                    types: $innerGeneticTypes,
                    array: $innerGeneticIsArray ? ArrayVariant::LIST : ArrayVariant::NONE,
                    nullable: false,
                    optional: false,
                    generic: true,
                );
            } else {
                $propertySchema = $this->inspectParameter($typeMap, $reflectionParameter);
            }

            $typeSchema->properties[$propertyName] = $propertySchema;
        }

        if ($discriminator !== null) {
            $property = $this->discToPropertySchema($typeMap, $discriminator, $typeName);
            $typeSchema->properties[$property->name] = $property;
        }

        return $typeSchema;
    }

    private function inspectParameter(TypeMap $typeMap, ReflectionParameter $reflectionParameter): PropertySchema
    {
        $propertyName = $reflectionParameter->getName();

        $reflectionType = $reflectionParameter->getType();
        if ($reflectionType instanceof ReflectionNamedType && $reflectionType->getName() !== 'array') {
            $typeName = $reflectionType->getName();
            if ($typeName === 'self') {
                $typeName = $reflectionParameter->getDeclaringClass()?->getName() ?? throw new GenException('Unknown type');
            }

            $deprecated = $reflectionParameter->getAttributes(JetBrainsDeprecated::class) !== [] || $reflectionParameter->getAttributes(Deprecated::class) !== [];
            if (!$deprecated) {
                try {
                    $deprecated = str_contains((string) $reflectionParameter->getDeclaringClass()?->getProperty($propertyName)->getDocComment(), '@deprecated');
                } catch (ReflectionException) {
                }
            }

            return new PropertySchema(
                name: $propertyName,
                types: $this->phpDocInspector->resolveTypeSchemas($typeMap, [$typeName], $reflectionParameter),
                array: ArrayVariant::NONE,
                nullable: $reflectionType->allowsNull(),
                optional: $reflectionParameter->isDefaultValueAvailable(),
                deprecated: $deprecated,
                defaultValue: $reflectionParameter->isDefaultValueAvailable() ? $reflectionParameter->getDefaultValue() : null,
            );
        }

        return $this->phpDocInspector->inspect($typeMap, $propertyName, $reflectionParameter);
    }

    private function discToPropertySchema(TypeMap $typeMap, HydratorMap $hydratorMap, string $typeName): PropertySchema
    {
        $map = array_flip($hydratorMap->matrix);
        $propertyName = $hydratorMap->property;
        $discriminatorValue = $map[$typeName] ?? throw new LogicException("Cannot find '$typeName' in " . json_encode($map));

        // check
        $originalProperty = $typeMap[$typeName]->properties[$propertyName] ?? null;
        $defaultValue = $originalDefaultValue = $originalProperty?->defaultValue;
        if ($defaultValue instanceof BackedEnum) {
            $defaultValue = $defaultValue->value;
        }

        if ($originalProperty !== null && $defaultValue !== $discriminatorValue) {
            throw new GenException(
                "Invalid discriminator value in $typeName::\$$propertyName " .
                "('$discriminatorValue' [" . get_debug_type($discriminatorValue) . "] vs " .
                "'$defaultValue' [" . get_debug_type($defaultValue) . "])",
            );
        }

        return new PropertySchema(
            name: $propertyName,
            types: [
                'string' => $typeMap['string'] ?? throw new LogicException(),
            ],
            array: ArrayVariant::NONE,
            nullable: false,
            optional: false,
            defaultValue: $originalDefaultValue,
            discriminatorValue: $discriminatorValue,
        );
    }
}
