<?php

declare(strict_types=1);

namespace UXF\Gen\Inspector;

use Nette\Utils\Strings;
use ReflectionMethod;
use ReflectionNamedType;
use UXF\Gen\Inspector\Schema\ArrayVariant;
use UXF\Gen\Inspector\Schema\PropertySchema;

final readonly class MethodInspector
{
    public function __construct(private PhpDocInspector $phpDocInspector)
    {
    }

    public function inspectReturnType(
        TypeMap $typeMap,
        string $propertyName,
        ReflectionMethod $reflectionMethod,
    ): PropertySchema {
        $reflectionReturnType = $reflectionMethod->getReturnType();
        if ($reflectionReturnType instanceof ReflectionNamedType && $reflectionReturnType->getName() !== 'array') {
            $typeName = $reflectionReturnType->getName();

            return $this->resolveGeneric($typeMap, $reflectionMethod, $typeName) ?? new PropertySchema(
                name: $propertyName,
                types: $this->phpDocInspector->resolveTypeSchemas($typeMap, [$typeName], null),
                array: ArrayVariant::NONE,
                nullable: $reflectionReturnType->allowsNull(),
                optional: false,
                defaultValue: false,
            );
        }

        return $this->phpDocInspector->inspect($typeMap, $propertyName, $reflectionMethod);
    }

    private function resolveGeneric(TypeMap $typeMap, ReflectionMethod $reflectionMethod, string $typeName): ?PropertySchema
    {
        $phpDoc = $reflectionMethod->getDocComment();
        if ($phpDoc === false) {
            return null;
        }

        $m = Strings::match($phpDoc, '/@return .*<([\w\d]+)(\[\])?>/i');
        if ($m === null) {
            return null;
        }

        $innerTypeName = $this->phpDocInspector->resolveFullClassName($reflectionMethod, $m[1]);

        $fullTypeName = "$typeName:$innerTypeName" . (isset($m[2]) ? '[]' : '');
        if (!isset($typeMap[$fullTypeName])) {
            $types = $this->phpDocInspector->resolveTypeSchemas($typeMap, [$fullTypeName], null);
        } else {
            $types = [
                $typeName => $typeMap[$fullTypeName],
            ];
        }

        return new PropertySchema(
            name: '',
            types: $types,
            array: ArrayVariant::NONE,
            nullable: false,
            optional: false,
        );
    }
}
