<?php

declare(strict_types=1);

namespace UXF\Gen\Inspector\Schema;

final class RouteSchema
{
    /**
     * @param PropertySchema[] $requestQueries
     * @param PropertySchema[] $requestHeaders
     * @param array<string, PropertySchema> $pathParams
     * @param array<string, bool> $methods
     */
    public function __construct(
        public string $name,
        public string $path,
        public string $controller,
        public ?PropertySchema $requestBody,
        public array $requestQueries,
        public array $requestHeaders,
        public array $pathParams,
        public ?PropertySchema $response,
        public string $description,
        public bool $deprecated,
        public array $methods = [],
    ) {
    }
}
