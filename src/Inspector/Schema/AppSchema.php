<?php

declare(strict_types=1);

namespace UXF\Gen\Inspector\Schema;

use UXF\Gen\Inspector\TypeMap;

final class AppSchema
{
    /**
     * @param RouteSchema[] $routes
     */
    public function __construct(public array $routes, public TypeMap $types)
    {
    }
}
