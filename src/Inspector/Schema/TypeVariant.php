<?php

declare(strict_types=1);

namespace UXF\Gen\Inspector\Schema;

enum TypeVariant
{
    case SIMPLE;
    case OBJECT;
    case GENERIC_OBJECT;
    case DOCTRINE_OBJECT;
    case ENUM;
    case DISCRIMINATOR_OBJECT;
}
