<?php

declare(strict_types=1);

namespace UXF\Gen\Inspector;

use ArrayAccess;
use IteratorAggregate;
use Nette\Utils\Strings;
use Ramsey\Uuid\UuidInterface;
use RecursiveArrayIterator;
use Traversable;
use UXF\Gen\Inspector\Schema\TypeSchema;
use UXF\Gen\Inspector\Schema\TypeVariant;

/**
 * @implements ArrayAccess<string, TypeSchema>
 * @implements IteratorAggregate<string, TypeSchema>
 */
final class TypeMap implements ArrayAccess, IteratorAggregate
{
    /**
     * @param array<string, TypeSchema> $map
     */
    public function __construct(private array $map = [])
    {
        $this->map['bool'] = new TypeSchema('bool', TypeVariant::SIMPLE);
        $this->map['int'] = new TypeSchema('int', TypeVariant::SIMPLE);
        $this->map['float'] = new TypeSchema('float', TypeVariant::SIMPLE);
        $this->map['string'] = new TypeSchema('string', TypeVariant::SIMPLE);
        $this->map[UuidInterface::class] = new TypeSchema(UuidInterface::class, TypeVariant::SIMPLE);
        $this->map['mixed'] = new TypeSchema('mixed', TypeVariant::SIMPLE);
    }

    public function offsetExists(mixed $offset): bool
    {
        return isset($this->map[$offset]);
    }

    public function offsetGet(mixed $offset): TypeSchema
    {
        return $this->map[$offset];
    }

    public function offsetSet(mixed $offset, mixed $value): void
    {
        $this->map[$offset] = $value;
    }

    public function offsetUnset(mixed $offset): void
    {
        unset($this->map[$offset]);
    }

    public function getIterator(): Traversable
    {
        return new RecursiveArrayIterator($this->map);
    }

    public function sort(): void
    {
        // fix entity # sort
        uksort(
            $this->map,
            static fn (string $a, string $b) => Strings::replace($a, '/.*#/') <=> Strings::replace($b, '/.*#/'),
        );
    }
}
