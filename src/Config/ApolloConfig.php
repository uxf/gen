<?php

declare(strict_types=1);

namespace UXF\Gen\Config;

final readonly class ApolloConfig
{
    /**
     * @param string[] $destinations
     * @param string[] $prepends
     */
    public function __construct(
        public string $name,
        public array $destinations,
        public string $pathPattern,
        public array $prepends,
    ) {
    }
}
