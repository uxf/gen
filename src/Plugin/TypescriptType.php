<?php

declare(strict_types=1);

namespace UXF\Gen\Plugin;

final readonly class TypescriptType
{
    public function __construct(
        public string $name,
        public string $type,
    ) {
    }
}
