<?php

declare(strict_types=1);

namespace UXF\Gen\Plugin;

use UXF\Gen\Inspector\Schema\AppSchema;
use UXF\Gen\Inspector\TypeMap;

interface InspectorPlugin
{
    public function pre(string $configName, TypeMap $typeMap): void;
    public function post(string $configName, AppSchema $appSchema): void;
}
