<?php

declare(strict_types=1);

namespace UXF\Gen\Bridge;

use LogicException;
use UXF\Core\Type\BankAccountNumberCze;
use UXF\Core\Type\Currency;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Decimal;
use UXF\Core\Type\Email;
use UXF\Core\Type\Money;
use UXF\Core\Type\NationalIdentificationNumberCze;
use UXF\Core\Type\Phone;
use UXF\Core\Type\Time;
use UXF\Core\Type\Url;
use UXF\Gen\Inspector\Schema\AppSchema;
use UXF\Gen\Inspector\Schema\ArrayVariant;
use UXF\Gen\Inspector\Schema\PropertySchema;
use UXF\Gen\Inspector\Schema\TypeSchema;
use UXF\Gen\Inspector\Schema\TypeVariant;
use UXF\Gen\Inspector\TypeMap;
use UXF\Gen\Plugin\InspectorPlugin;

final readonly class UxfCoreInspectorPlugin implements InspectorPlugin
{
    public function pre(string $configName, TypeMap $typeMap): void
    {
        $typeMap[Date::class] = new TypeSchema(Date::class, TypeVariant::SIMPLE);
        $typeMap[DateTime::class] = new TypeSchema(DateTime::class, TypeVariant::SIMPLE);
        $typeMap[Time::class] = new TypeSchema(Time::class, TypeVariant::SIMPLE);
        $typeMap[Phone::class] = new TypeSchema(Phone::class, TypeVariant::SIMPLE);
        $typeMap[Email::class] = new TypeSchema(Email::class, TypeVariant::SIMPLE);
        $typeMap[Decimal::class] = new TypeSchema(Decimal::class, TypeVariant::SIMPLE);
        $typeMap[Url::class] = new TypeSchema(Url::class, TypeVariant::SIMPLE);
        $typeMap[NationalIdentificationNumberCze::class] = new TypeSchema(NationalIdentificationNumberCze::class, TypeVariant::SIMPLE);
        $typeMap[BankAccountNumberCze::class] = new TypeSchema(BankAccountNumberCze::class, TypeVariant::SIMPLE);
        $typeMap[Currency::class] = $currency = new TypeSchema(Currency::class, TypeVariant::ENUM);

        $string = $typeMap['string'] ?? throw new LogicException();
        $typeMap[Money::class] = new TypeSchema(Money::class, TypeVariant::OBJECT, Money::class, [
            'amount' => new PropertySchema(
                name: 'amount',
                types: [
                    'string' => $string,
                ],
                array: ArrayVariant::NONE,
                nullable: false,
                optional: false,
            ),
            'currency' => new PropertySchema(
                name: 'currency',
                types: [
                    Currency::class => $currency,
                ],
                array: ArrayVariant::NONE,
                nullable: false,
                optional: false,
            ),
        ]);
    }

    public function post(string $configName, AppSchema $appSchema): void
    {
    }

    public static function getDefaultPriority(): int
    {
        return 10;
    }
}
