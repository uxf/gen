<?php

declare(strict_types=1);

namespace UXF\Gen\Bridge;

use UXF\Core\Type\BankAccountNumberCze;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Decimal;
use UXF\Core\Type\Email;
use UXF\Core\Type\NationalIdentificationNumberCze;
use UXF\Core\Type\Phone;
use UXF\Core\Type\Time;
use UXF\Core\Type\Url;
use UXF\Gen\Inspector\Schema\TypeSchema;
use UXF\Gen\Inspector\Schema\TypeVariant;
use UXF\Gen\Plugin\TypeConverterPlugin;
use UXF\Gen\Plugin\TypescriptType;

final readonly class UxfCoreTypeConverterPlugin implements TypeConverterPlugin
{
    /**
     * @param array<string, string> $types
     */
    public function __construct(
        private array $types,
    ) {
    }

    public function convertToTypescript(TypeSchema $typeSchema): ?TypescriptType
    {
        if ($typeSchema->variant !== TypeVariant::SIMPLE) {
            return null;
        }

        return match ($typeSchema->name) {
            Date::class => new TypescriptType('Date', $this->types['Date'] ?? 'string'),
            DateTime::class => new TypescriptType('DateTime', $this->types['DateTime'] ?? 'string'),
            Decimal::class => new TypescriptType('Decimal', $this->types['Decimal'] ?? 'string'),
            Url::class => new TypescriptType('Url', $this->types['Url'] ?? 'string'),
            Time::class => new TypescriptType('Time', $this->types['Time'] ?? 'string'),
            Phone::class => new TypescriptType('Phone', $this->types['Phone'] ?? 'string'),
            Email::class => new TypescriptType('Email', $this->types['Email'] ?? 'string'),
            NationalIdentificationNumberCze::class => new TypescriptType('NationalIdentificationNumberCze', $this->types['NationalIdentificationNumberCze'] ?? 'string'),
            BankAccountNumberCze::class => new TypescriptType('BankAccountNumberCze', $this->types['BankAccountNumberCze'] ?? 'string'),
            default => null,
        };
    }

    /**
     * @inheritDoc
     */
    public function convertToOpenApi(TypeSchema $typeSchema): ?array
    {
        if ($typeSchema->variant !== TypeVariant::SIMPLE) {
            return null;
        }

        return match ($typeSchema->name) {
            Date::class => [
                'type' => 'string',
                'format' => 'date',
            ],
            DateTime::class => [
                'type' => 'string',
                'format' => 'date-time',
            ],
            Decimal::class => [
                'type' => 'string',
            ],
            Url::class => [
                'type' => 'string',
                'format' => 'url',
            ],
            Time::class => [
                'type' => 'string',
                'pattern' => '^\d{2}:\d{2}:\d{2}$',
            ],
            Phone::class => [
                'type' => 'string',
            ],
            Email::class => [
                'type' => 'string',
                'format' => 'email',
            ],
            NationalIdentificationNumberCze::class => [
                'type' => 'string',
            ],
            BankAccountNumberCze::class => [
                'type' => 'string',
            ],
            default => null,
        };
    }

    public static function getDefaultPriority(): int
    {
        return 10;
    }
}
