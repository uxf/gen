import { BaseDataSource } from "./BaseDataSource";

// 
export const Currency = {
    Aed: 'AED',
    Afn: 'AFN',
    All: 'ALL',
    Amd: 'AMD',
    Ang: 'ANG',
    Aoa: 'AOA',
    Ars: 'ARS',
    Aud: 'AUD',
    Awg: 'AWG',
    Azn: 'AZN',
    Bam: 'BAM',
    Bbd: 'BBD',
    Bdt: 'BDT',
    Bgn: 'BGN',
    Bhd: 'BHD',
    Bif: 'BIF',
    Bmd: 'BMD',
    Bnd: 'BND',
    Bob: 'BOB',
    Bov: 'BOV',
    Brl: 'BRL',
    Bsd: 'BSD',
    Btn: 'BTN',
    Bwp: 'BWP',
    Byn: 'BYN',
    Bzd: 'BZD',
    Cad: 'CAD',
    Cdf: 'CDF',
    Che: 'CHE',
    Chf: 'CHF',
    Chw: 'CHW',
    Clf: 'CLF',
    Clp: 'CLP',
    Cny: 'CNY',
    Cop: 'COP',
    Cou: 'COU',
    Crc: 'CRC',
    Cuc: 'CUC',
    Cup: 'CUP',
    Cve: 'CVE',
    Czk: 'CZK',
    Djf: 'DJF',
    Dkk: 'DKK',
    Dop: 'DOP',
    Dzd: 'DZD',
    Egp: 'EGP',
    Ern: 'ERN',
    Etb: 'ETB',
    Eur: 'EUR',
    Fjd: 'FJD',
    Fkp: 'FKP',
    Gbp: 'GBP',
    Gel: 'GEL',
    Ghs: 'GHS',
    Gip: 'GIP',
    Gmd: 'GMD',
    Gnf: 'GNF',
    Gtq: 'GTQ',
    Gyd: 'GYD',
    Hkd: 'HKD',
    Hnl: 'HNL',
    Htg: 'HTG',
    Huf: 'HUF',
    Idr: 'IDR',
    Ils: 'ILS',
    Inr: 'INR',
    Iqd: 'IQD',
    Irr: 'IRR',
    Isk: 'ISK',
    Jmd: 'JMD',
    Jod: 'JOD',
    Jpy: 'JPY',
    Kes: 'KES',
    Kgs: 'KGS',
    Khr: 'KHR',
    Kmf: 'KMF',
    Kpw: 'KPW',
    Krw: 'KRW',
    Kwd: 'KWD',
    Kyd: 'KYD',
    Kzt: 'KZT',
    Lak: 'LAK',
    Lbp: 'LBP',
    Lkr: 'LKR',
    Lrd: 'LRD',
    Lsl: 'LSL',
    Lyd: 'LYD',
    Mad: 'MAD',
    Mdl: 'MDL',
    Mga: 'MGA',
    Mkd: 'MKD',
    Mmk: 'MMK',
    Mnt: 'MNT',
    Mop: 'MOP',
    Mru: 'MRU',
    Mur: 'MUR',
    Mvr: 'MVR',
    Mwk: 'MWK',
    Mxn: 'MXN',
    Mxv: 'MXV',
    Myr: 'MYR',
    Mzn: 'MZN',
    Nad: 'NAD',
    Ngn: 'NGN',
    Nio: 'NIO',
    Nok: 'NOK',
    Npr: 'NPR',
    Nzd: 'NZD',
    Omr: 'OMR',
    Pab: 'PAB',
    Pen: 'PEN',
    Pgk: 'PGK',
    Php: 'PHP',
    Pkr: 'PKR',
    Pln: 'PLN',
    Pyg: 'PYG',
    Qar: 'QAR',
    Ron: 'RON',
    Rsd: 'RSD',
    Rub: 'RUB',
    Rwf: 'RWF',
    Sar: 'SAR',
    Sbd: 'SBD',
    Scr: 'SCR',
    Sdg: 'SDG',
    Sek: 'SEK',
    Sgd: 'SGD',
    Shp: 'SHP',
    Sle: 'SLE',
    Sll: 'SLL',
    Sos: 'SOS',
    Srd: 'SRD',
    Ssp: 'SSP',
    Stn: 'STN',
    Svc: 'SVC',
    Syp: 'SYP',
    Szl: 'SZL',
    Thb: 'THB',
    Tjs: 'TJS',
    Tmt: 'TMT',
    Tnd: 'TND',
    Top: 'TOP',
    Try: 'TRY',
    Ttd: 'TTD',
    Twd: 'TWD',
    Tzs: 'TZS',
    Uah: 'UAH',
    Ugx: 'UGX',
    Usd: 'USD',
    Usn: 'USN',
    Uyi: 'UYI',
    Uyu: 'UYU',
    Uyw: 'UYW',
    Uzs: 'UZS',
    Ved: 'VED',
    Ves: 'VES',
    Vnd: 'VND',
    Vuv: 'VUV',
    Wst: 'WST',
    Xaf: 'XAF',
    Xcd: 'XCD',
    Xof: 'XOF',
    Xpf: 'XPF',
    Yer: 'YER',
    Zar: 'ZAR',
    Zmw: 'ZMW',
    Zwl: 'ZWL',
} as const;
export type Currency = 'AED' | 'AFN' | 'ALL' | 'AMD' | 'ANG' | 'AOA' | 'ARS' | 'AUD' | 'AWG' | 'AZN' | 'BAM' | 'BBD' | 'BDT' | 'BGN' | 'BHD' | 'BIF' | 'BMD' | 'BND' | 'BOB' | 'BOV' | 'BRL' | 'BSD' | 'BTN' | 'BWP' | 'BYN' | 'BZD' | 'CAD' | 'CDF' | 'CHE' | 'CHF' | 'CHW' | 'CLF' | 'CLP' | 'CNY' | 'COP' | 'COU' | 'CRC' | 'CUC' | 'CUP' | 'CVE' | 'CZK' | 'DJF' | 'DKK' | 'DOP' | 'DZD' | 'EGP' | 'ERN' | 'ETB' | 'EUR' | 'FJD' | 'FKP' | 'GBP' | 'GEL' | 'GHS' | 'GIP' | 'GMD' | 'GNF' | 'GTQ' | 'GYD' | 'HKD' | 'HNL' | 'HTG' | 'HUF' | 'IDR' | 'ILS' | 'INR' | 'IQD' | 'IRR' | 'ISK' | 'JMD' | 'JOD' | 'JPY' | 'KES' | 'KGS' | 'KHR' | 'KMF' | 'KPW' | 'KRW' | 'KWD' | 'KYD' | 'KZT' | 'LAK' | 'LBP' | 'LKR' | 'LRD' | 'LSL' | 'LYD' | 'MAD' | 'MDL' | 'MGA' | 'MKD' | 'MMK' | 'MNT' | 'MOP' | 'MRU' | 'MUR' | 'MVR' | 'MWK' | 'MXN' | 'MXV' | 'MYR' | 'MZN' | 'NAD' | 'NGN' | 'NIO' | 'NOK' | 'NPR' | 'NZD' | 'OMR' | 'PAB' | 'PEN' | 'PGK' | 'PHP' | 'PKR' | 'PLN' | 'PYG' | 'QAR' | 'RON' | 'RSD' | 'RUB' | 'RWF' | 'SAR' | 'SBD' | 'SCR' | 'SDG' | 'SEK' | 'SGD' | 'SHP' | 'SLE' | 'SLL' | 'SOS' | 'SRD' | 'SSP' | 'STN' | 'SVC' | 'SYP' | 'SZL' | 'THB' | 'TJS' | 'TMT' | 'TND' | 'TOP' | 'TRY' | 'TTD' | 'TWD' | 'TZS' | 'UAH' | 'UGX' | 'USD' | 'USN' | 'UYI' | 'UYU' | 'UYW' | 'UZS' | 'VED' | 'VES' | 'VND' | 'VUV' | 'WST' | 'XAF' | 'XCD' | 'XOF' | 'XPF' | 'YER' | 'ZAR' | 'ZMW' | 'ZWL';

// UXF\GenTests\Project\FunZone\Entity\EnumNumeric
export const EnumNumeric = {
    Zero: 0,
    One: 1,
} as const;
export type EnumNumeric = 0 | 1;

// UXF\GenTests\Project\FunZone\Entity\Type
export const Type = {
    New: 'NEW',
    Old: 'OLD',
} as const;
export type Type = 'NEW' | 'OLD';

type UUID = string;

type BankAccountNumberCze = string;

type Date = `${number}-${number}-${number}`;

type DateTime = `${number}-${number}-${number}T${number}:${number}:${number}+${number}:${number}`;

type Decimal = string;

type Email = string;

// UXF\Core\Type\Money
export interface Money {
    amount: XString;
    currency: Currency;
}

type NationalIdentificationNumberCze = string;

type Phone = string;

type Time = `${number}:${number}:${number}`;

type Url = string;

// UXF\GenTests\Project\FunZone\Http\Request\ArticleRequestBody
export interface FunArticleRequestBody {
    title: XString;
    type: Type;
    enumNumeric: EnumNumeric;
    publishedAt: DateTime;
    priority: Int;
    score: Float;
    active: Bool;
    category: Int;
    enumEntity: Type;
    tags: Array<Int>;
    file: UUID;
    files: Array<UUID>;
    content?: XString | null;
}

// UXF\GenTests\Project\FunZone\Http\Request\ArticleRequestHeader
export interface FunArticleRequestHeader {
    acceptLanguage: XString;
}

// UXF\GenTests\Project\FunZone\Http\Request\ArticleRequestQuery
export interface FunArticleRequestQuery {
    title?: XString | null;
    type?: Type | null;
    publishedAt?: DateTime | null;
    priority?: Int | null;
    score?: Float | null;
    active?: Bool | null;
    category?: Int | null;
    enumEntity?: Type | null;
    tags?: Array<Int>;
}

// UXF\GenTests\Project\FunZone\Http\Request\Family\Activity
export type FunActivity = FunOrienteering | FunParagliding;

// UXF\GenTests\Project\FunZone\Http\Request\Family\Orienteering
export interface FunOrienteering {
    card: Int;
    type: "o";
}

// UXF\GenTests\Project\FunZone\Http\Request\Family\Paragliding
export interface FunParagliding {
    glider: XString;
    type: "p";
}

// UXF\GenTests\Project\FunZone\Http\Request\JumpRequestQuery
export interface FunJumpRequestQuery {
    article?: Int | null;
}

// UXF\GenTests\Project\FunZone\Http\Request\MacRequestBody
/** @deprecated */
export interface FunMacRequestBody {
    id: Int;
    name: XString;
}

// UXF\GenTests\Project\FunZone\Http\Request\PersonRequestBody
/** @deprecated */
export interface FunPersonRequestBody {
    /** @deprecated */
    string: XString;
    /** @deprecated */
    int: Int;
    float: Float;
    stringArray: Array<XString>;
    stringList: Array<XString>;
    stringIndexedArray: Record<string, XString>;
    obj: FunMacRequestBody;
    entity: Int;
    /** @deprecated */
    objArray: Array<FunMacRequestBody>;
    /** @deprecated */
    objIndexedArray: Record<string, FunMacRequestBody>;
    stringNull: XString | null;
    intNull: Int | null;
    floatNull: Float | null;
    stringArrayNull: Array<XString> | null;
    stringListNull: Array<XString> | null;
    stringIndexedArrayNull: Record<string, XString> | null;
    objNull: FunMacRequestBody | null;
    entityNull: Int | null;
    objArrayNull: Array<FunMacRequestBody> | null;
    objIndexedArrayNull: Record<string, FunMacRequestBody> | null;
    simpleUnion: Int | XString;
    nullableSimpleUnion: Int | XString | null;
    unionWithObject: FunMacRequestBody | Int | XString;
    arrayOfUnion: Array<FunMacRequestBody | XString>;
    listOfUnion: Array<FunMacRequestBody | XString>;
    simpleArrayOfUnion: Array<FunMacRequestBody | XString>;
    stringOptional?: XString;
    intOptional?: Int;
    floatOptional?: Float;
    stringArrayOptional?: Array<XString>;
    stringListOptional?: Array<XString>;
    stringIndexedArrayOptional?: Record<string, XString>;
    objArrayOptional?: Array<FunMacRequestBody>;
    objIndexedArrayOptional?: Record<string, FunMacRequestBody>;
    stringNullOptional?: XString | null;
    intNullOptional?: Int | null;
    floatNullOptional?: Float | null;
    stringArrayNullOptional?: Array<XString> | null;
    stringListNullOptional?: Array<XString> | null;
    stringIndexedArrayNullOptional?: Record<string, XString> | null;
    objNullOptional?: FunMacRequestBody | null;
    entityNullOptional?: Int | null;
    objArrayNullOptional?: Array<FunMacRequestBody> | null;
    objIndexedArrayNullOptional?: Record<string, FunMacRequestBody> | null;
    stringNullPatch?: XString | null;
    intNullPatch?: Int | null;
    floatNullPatch?: Float | null;
    stringArrayNullPatch?: Array<XString> | null;
    stringListNullPatch?: Array<XString> | null;
    stringIndexedArrayNullPatch?: Record<string, XString> | null;
    objNullPatch?: FunMacRequestBody | null;
    entityNullPatch?: Int | null;
    objArrayNullPatch?: Array<FunMacRequestBody> | null;
    objIndexedArrayNullPatch?: Record<string, FunMacRequestBody> | null;
    date?: Date | null;
    dateTime?: DateTime | null;
    time?: Time | null;
    phone?: Phone | null;
    email?: Email | null;
    nicCze?: NationalIdentificationNumberCze | null;
    banCze?: BankAccountNumberCze | null;
    money?: Money | null;
    decimal?: Decimal | null;
    url?: Url | null;
}

// UXF\GenTests\Project\FunZone\Http\Response\ArticleResponse
export interface FunArticleResponse {
    id: Int;
    title: XString;
    type: Type | null;
    content: XString | null;
    createdAt: Date;
    publishedAt: DateTime | null;
    priority: Int;
    score: Float;
    active: Bool;
    category: FunCategoryResponse;
    tags: Array<FunTagResponse>;
    tagList: Array<FunTagResponse>;
    stringIndexedTags: Record<string, FunTagResponse>;
    intIndexedTags: Record<string, FunTagResponse>;
    arrayOfUnion: Array<FunTagResponse | XString>;
    bracketArrayOfUnion: Array<FunTagResponse | XString>;
    indexedArrayOfUnion: Record<string, FunTagResponse | XString>;
    union: Int | XString;
    uuid: UUID;
}

// UXF\GenTests\Project\FunZone\Http\Response\CategoryResponse
export interface FunCategoryResponse {
    id: Int;
    name: XString;
}

// UXF\GenTests\Project\FunZone\Http\Response\Generic\GenericResponseBody
export interface FunGenericResponseBody<T> {
    success: Bool;
    data: T;
}

// UXF\GenTests\Project\FunZone\Http\Response\TagResponse
export interface FunTagResponse {
    id: Int;
    name: XString;
}

type Bool = boolean;

type Float = number;

type Int = number;

type Mixed = any;

type XString = string;



export class FunDataSource extends BaseDataSource {
    public articleAll(input: { query: FunArticleRequestQuery }): Promise<Array<FunArticleResponse>> {
        return this.get(`/api/article`, input.query);
    }

    public articleDoubleQuery(input: { query: FunArticleRequestQuery & FunJumpRequestQuery }): Promise<Mixed> {
        return this.get(`/api/article/double-query`, input.query);
    }

    public articleCreate(input: { body: FunArticleRequestBody }): Promise<FunArticleResponse> {
        return this.post(`/api/article`, input.body);
    }

    public articleRead(input: { path: { article: Int } }): Promise<FunArticleResponse> {
        return this.get(`/api/article/${input.path.article}`, undefined);
    }

    public articleUpdate(input: { path: { article: Int }, body: FunArticleRequestBody }): Promise<FunArticleResponse> {
        return this.put(`/api/article/${input.path.article}`, input.body);
    }

    public articleDelete(input: { path: { article: Int } }): Promise<Mixed> {
        return this.delete(`/api/article/${input.path.article}`);
    }

    public invoke(input: { path: { type: Type } }): Promise<Array<FunCategoryResponse | FunTagResponse>> {
        return this.get(`/api/article/invoke/${input.path.type}`, undefined);
    }

    public personCreate(input: { body: FunPersonRequestBody }): Promise<Mixed> {
        return this.post(`/api/person`, input.body);
    }

    public unionDetail(input: { body: FunActivity }): Promise<FunActivity> {
        return this.post(`/api/union`, input.body);
    }

    public unionList(input: { body: Array<FunActivity> }): Promise<Array<FunActivity>> {
        return this.post(`/api/unions`, input.body);
    }

    public entity(input: { path: { id: Int, uuid: UUID, string: Type, int: EnumNumeric } }): Promise<Mixed> {
        return this.get(`/api/entity/${input.path.id}/${input.path.uuid}/${input.path.string}/${input.path.int}`, undefined);
    }

    public genericOne(): Promise<FunGenericResponseBody<FunTagResponse>> {
        return this.get(`/api/gen-one`, undefined);
    }

    public genericTwo(): Promise<FunGenericResponseBody<Array<FunTagResponse>>> {
        return this.get(`/api/gen-two`, undefined);
    }

    public genericThree(): Promise<FunGenericResponseBody<Bool>> {
        return this.get(`/api/gen-three`, undefined);
    }

    public mixed(input: { body: Mixed }): Promise<Mixed> {
        return this.post(`/api/mixed`, input.body);
    }

    public fullRead(input: { path: { article: Int }, query: FunArticleRequestQuery, body: FunArticleRequestBody }): Promise<FunArticleResponse> {
        return this.get(`/api/full/${input.path.article}`, input.body);
    }

    public fullWrite(input: { path: { article: Int }, query: FunArticleRequestQuery, body: FunArticleRequestBody }): Promise<FunArticleResponse> {
        return this.patch(`/api/full/${input.path.article}`, input.body);
    }

    public plugin(): Promise<any> {
        return this.get(`/plugin`, undefined);
    }

}

