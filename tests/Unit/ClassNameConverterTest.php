<?php

declare(strict_types=1);

namespace UXF\GenTests\Unit;

use PHPUnit\Framework\TestCase;
use UXF\CMS\Http\Request\Cms\CreateUserRequestBody;
use UXF\DataGrid\Schema\DataGridSchema;
use UXF\Gen\Generator\SimpleClassNameConverter;

class ClassNameConverterTest extends TestCase
{
    public function test(): void
    {
        $converter = new SimpleClassNameConverter();

        self::assertSame('SomeAnimal', $converter->convert('App\SomeZone\Animal'));
        self::assertSame('Some', $converter->resolveTag('App\SomeZone\Animal'));

        self::assertSame('Animal', $converter->convert('App\AnimalZone\Animal'));
        self::assertSame('Animal', $converter->resolveTag('App\AnimalZone\Animal'));

        self::assertSame('CMSCreateUserRequestBody', $converter->convert(CreateUserRequestBody::class));
        self::assertNull($converter->resolveTag(CreateUserRequestBody::class));

        self::assertSame('DataGridSchema', $converter->convert(DataGridSchema::class));
        self::assertNull($converter->resolveTag(DataGridSchema::class));
    }
}
