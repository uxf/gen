<?php

declare(strict_types=1);

namespace UXF\GenTests\Project\FunZone\Http\Request;

use JetBrains\PhpStorm\Deprecated;

#[Deprecated]
final readonly class MacRequestBody
{
    public function __construct(
        public int $id,
        public string $name,
    ) {
    }
}
