<?php

declare(strict_types=1);

namespace UXF\GenTests\Project\FunZone\Http\Request\Family;

final readonly class Club
{
    /**
     * @param Activity[] $activities
     * @param Sport[] $sports
     */
    public function __construct(
        public array $activities,
        public array $sports,
        public Activity $activity,
        public Sport $sport,
    ) {
    }
}
