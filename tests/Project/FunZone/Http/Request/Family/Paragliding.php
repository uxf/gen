<?php

declare(strict_types=1);

namespace UXF\GenTests\Project\FunZone\Http\Request\Family;

class Paragliding extends Sport
{
    public function __construct(
        public readonly string $glider,
        public readonly string $type = 'p',
    ) {
    }
}
