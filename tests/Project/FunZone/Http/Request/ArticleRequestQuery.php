<?php

declare(strict_types=1);

namespace UXF\GenTests\Project\FunZone\Http\Request;

use UXF\Core\Type\DateTime;
use UXF\GenTests\Project\FunZone\Entity\Category;
use UXF\GenTests\Project\FunZone\Entity\EntityWithEnumId;
use UXF\GenTests\Project\FunZone\Entity\Tag;
use UXF\GenTests\Project\FunZone\Entity\Type;

final readonly class ArticleRequestQuery
{
    /**
     * @param Tag[] $tags
     */
    public function __construct(
        public ?string $title = null,
        public ?Type $type = null,
        public ?DateTime $publishedAt = null,
        public ?int $priority = null,
        public ?float $score = null,
        public ?bool $active = null,
        public ?Category $category = null,
        public ?EntityWithEnumId $enumEntity = null,
        public array $tags = [],
    ) {
    }
}
