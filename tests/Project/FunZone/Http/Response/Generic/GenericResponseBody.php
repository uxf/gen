<?php

declare(strict_types=1);

namespace UXF\GenTests\Project\FunZone\Http\Response\Generic;

/**
 * @template T
 */
final readonly class GenericResponseBody
{
    /**
     * @param T $data
     */
    public function __construct(
        public bool $success,
        public mixed $data,
    ) {
    }
}
