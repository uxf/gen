<?php

declare(strict_types=1);

namespace UXF\GenTests\Project\FunZone\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

#[ORM\Entity]
class File
{
    #[ORM\ManyToOne, ORM\Id]
    public InnerFile $id;

    #[ORM\Column(type: 'uuid', unique: true)]
    public UuidInterface $uuid;

    #[ORM\Column]
    public Type $string = Type::NEW;

    #[ORM\Column]
    public EnumNumeric $int = EnumNumeric::ZERO;

    public function __construct(InnerFile $id, UuidInterface $uuid)
    {
        $this->id = $id;
        $this->uuid = $uuid;
    }
}
