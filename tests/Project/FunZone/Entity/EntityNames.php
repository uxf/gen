<?php

declare(strict_types=1);

namespace UXF\GenTests\Project\FunZone\Entity;

use UXF\Core\Attribute\Label;

enum EntityNames: string
{
    #[Label("Článek", "red")]
    case ARTICLE = Article::class;
    #[Label("Kategorie", "blue")]
    case CATEGORY = Category::class;
    #[Label("Štítek")]
    case TAG = Tag::class;
}
