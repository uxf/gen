<?php

declare(strict_types=1);

namespace UXF\GenTests\Project\FunZone\Controller;

use UXF\Core\Attribute\FromBody;
use UXF\GenTests\Project\FunZone\Http\Request\PersonRequestBody;

class PersonController
{
    public function create(#[FromBody] PersonRequestBody $body): void
    {
    }
}
