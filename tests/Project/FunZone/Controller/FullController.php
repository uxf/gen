<?php

declare(strict_types=1);

namespace UXF\GenTests\Project\FunZone\Controller;

use Nette\NotImplementedException;
use UXF\Core\Attribute\FromBody;
use UXF\Core\Attribute\FromQuery;
use UXF\GenTests\Project\FunZone\Entity\Article;
use UXF\GenTests\Project\FunZone\Http\Request\ArticleRequestBody;
use UXF\GenTests\Project\FunZone\Http\Request\ArticleRequestQuery;
use UXF\GenTests\Project\FunZone\Http\Response\ArticleResponse;

final readonly class FullController
{
    public function __invoke(
        Article $article,
        #[FromBody] ArticleRequestBody $body,
        #[FromQuery] ArticleRequestQuery $query,
    ): ArticleResponse {
        throw new NotImplementedException();
    }
}
