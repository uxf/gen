<?php

declare(strict_types=1);

namespace UXF\GenTests\Project\FunZone\Controller;

use Nette\NotImplementedException;
use UXF\GenTests\Project\FunZone\Http\Response\Generic\GenericResponseBody;
use UXF\GenTests\Project\FunZone\Http\Response\TagResponse;

class GenericController
{
    /**
     * @return GenericResponseBody<TagResponse>
     */
    public function one(): GenericResponseBody
    {
        throw new NotImplementedException();
    }

    /**
     * @return GenericResponseBody<TagResponse[]>
     */
    public function two(): GenericResponseBody
    {
        throw new NotImplementedException();
    }

    /**
     * @return GenericResponseBody<bool>
     */
    public function three(): GenericResponseBody
    {
        throw new NotImplementedException();
    }
}
