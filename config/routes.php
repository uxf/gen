<?php

declare(strict_types=1);

use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;
use UXF\Gen\Controller\OpenApiController;

return static function (RoutingConfigurator $routingConfigurator): void {
    $routingConfigurator->add('uxf_gen_json', '/api/doc/{area}.json')
        ->controller([OpenApiController::class, 'json'])
        ->methods(['GET']);

    $routingConfigurator->add('uxf_gen_ui', '/api/doc/{area}')
        ->controller([OpenApiController::class, 'ui'])
        ->defaults([
            'area' => null,
        ])
        ->methods(['GET']);
};
